## Getting and Cleaning Data Project
This repo contains project work for the Getting and Cleaning Data course under the Data Science Specialization on Coursera.

### Code
`run_analysis.R` Core code that downloads, unzips, and runs each part of the project.

#### Steps taken in this script
1. Merges the training and the test sets to create one data set.
2. Extracts only the measurements on the mean and standard deviation for each measurement.
3. Uses descriptive activity names to name the activities in the data set.
4. Appropriately labels the data set with descriptive variable names.
5. From the data set in step 4, creates a second, independent tidy data set with the average of each variable for each activity and each subject.


### Files:
1. `tidy.txt` - Final output of tidy data.
2. `CodeBook.md` - Description of data, variables, and transormations performed.
3. `UCI HAR Dataset\test` - data source for test data
4. `UCI HAR Dataset\train` - data source for training
6. `UCI HAR Dataset\features.txt` - list of features in the data set
7. `UCI HAR Dataset\activity_labels.txt` - list of activity codes and descriptions
